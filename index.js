var http = require('http');

function servidorSimple(request, response) {
  var cabeceras = {
    'Content-type': 'text/plain'
  };

  response.writeHead(200, cabeceras)
  response.end('Hello World');
}

http.createServer(servidorSimple).listen(8080);
console.log('Servidor Up');
